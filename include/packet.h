#include <stdint.h>

/// @defgroup Packet-API

/**\addtogroup Packet-API
 *  @{
 */

/// First byte of the two byte Start of Frame sequence.
#define MKR_START_0 0x21

/// Second byte of the two byte Start of Frame sequence.
#define MKR_START_1 0x22

//
// Public API
//

typedef struct {
    uint8_t len;
    uint8_t* data;
} Packet;

/// Create a new Packet on the heap/
Packet* packet_new();

/// Properly free a Packet allocated on the heap.
void packet_free(Packet* pkt);

/**
 * @brief Read a packet from `stream` and store it in `pkt`.
 *
 * Pre-Conditions:
 *
 * - The input stream is configured as a binary stream.
 * - `pkt` must be non-null.
 *
 * @param stream Pointer to the FILE object to read from.
 * @param pkt [out] Extracted packet.
 * @return int `1` if a valid packet is found in the stream, `0` otherwise.
 */
int packet_read(FILE* stream, Packet* pkt);

/**
 * @brief Produce a human readable view of a packet.
 *
 * Format is:
 *
 * `{ LEN } DATA ...`
 *
 * With the data being shown as hex bytes.
 *
 * @param pkt The packet to display.
 */
void packet_print(Packet* pkt);

//
// Internal API
//


/**
 * @brief Seek through the stream looking for the start of frame markers.
 *
 * If the two start of frame markers are found in order, the read
 * cursor will now be placed at the start of the length field of the message.
 *
 * Pre-Conditions:
 *
 * - The input stream is configured as a binary stream.
 *
 * @param stream Pointer to the FILE object to read from.
 * @return int `1` if a start of frame (SoF) marker is found, `0` otherwise.
 */
int seek_to_sof(FILE* stream);

/**
 *
 *
 * Return `1` if the length could be read, `0` otherwise.
 **/

/**
 * @brief Read the length byte from the stream.
 *
 * Pre-Conditions:
 *
 * - The input stream is configured as a binary stream.
 * - `seek_to_sof` has been called and the read cursor is already on the length
 *   byte.
 *
 * @param stream Pointer to the FILE object to read from.
 * @param data_len [out] The data-length field of the message.
 * @return int `1` if the length could be read, `0` otherwise.
 */
int read_len(FILE* stream, uint8_t* data_len);

/**
 * @brief Read the data field from the stream.
 *
 * Pre-Conditions:
 *
 * - The input stream is configured as a binary stream.
 * - The read cursor is positioned at the start of the data field.
 *   (i.e. the first byte to be read is the first data byte)
 * - The `len` field of `pkt` is set.
 *
 * @param stream Pointer to the FILE object to read from.
 * @param pkt [inout] Partially assembled packet. `len` must be set to the
 *  data length. `data` will be allocated and populated.
 * @return int `1` if the data could be read, `0` otherwise.
 */
int read_data(FILE* stream, Packet* pkt);

/** @}*/
