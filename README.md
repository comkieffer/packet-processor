# Packet Processor

[Docs](https://comkieffer.gitlab.io/packet-processor/index.html)

## Getting Started

The program uses the `meson` build system. To build, run the following commands:

``` console
$ meson setup builddir
$ cd builddir && ninja
```

To run the test suite, run

``` console
$ meson test
```

## packet-processor tool

### Input File Format

The program accepts binary input files containing a string of bytes. These will be processed to extract packets in the following format :

 Offset |   Value  |  Type   |    Meaning
 ------ | -------- | ------- | ----------------
 0      | 0x21     | (byte)  | start marker 0
 1      | 0x22     | (byte)  | start marker 1
 2      | (length) | (uint8) | length of payload
 3-258  | (array)  | (bytes) | payload

Sequences that do not follow this format are discarded.

### Output format

For each packet in the input stream, a new in printed to stdout. The packet length is enclosed in curly braces followed by the payload as a series of hexadecimal digits. Payload bytes are separated by spaces.

An example of the output is show below:

```
{  3} 41 42 43
{  4} 64 65 66 67
```

## Future Improvements

1) More complete testing: the current test suite only goes through the *happy path*. A more comprehensive test-suite which tests the error handling and recovery is required.

2) Prepare a docker image with all the dependencies pre-installed to speed up CI runs.
