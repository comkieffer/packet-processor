
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

extern "C" {
    #include "packet.h"
}

int count_packets(FILE* in_file) {
    int packet_count = 0;
    while (!feof(in_file)) {
        fprintf(stderr, ".");
        if (seek_to_sof(in_file)) {
            packet_count++;
        }
    }

    return packet_count;
}

TEST_CASE("seek_to_sof accurately identifies packet starts", "[packet]") {
    FILE* in_file_1 = fopen("data/2_packets", "rb");
    REQUIRE(in_file_1);
    REQUIRE(count_packets(in_file_1) == 2);

    FILE* in_file_2 = fopen("data/10_short_packets", "rb");
    REQUIRE(in_file_2);
    REQUIRE(count_packets(in_file_2) == 10);
}



TEST_CASE("get_data_len returns accurate lengths", "[packet]") {
    FILE* in_file = fopen("data/10_short_packets", "rb");
    REQUIRE(in_file);

    uint8_t data_len = 0;
    std::vector<uint8_t> lengths{3, 13, 12, 2, 9, 4, 10, 5, 6, 5};
    for (auto len: lengths) {
        REQUIRE(seek_to_sof(in_file) == 1);
        REQUIRE(read_len(in_file, &data_len) == 1);
        REQUIRE(int(data_len) == len);
    }
}
