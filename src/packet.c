#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "packet.h"

Packet* packet_new() {
    Packet* pkt = malloc(sizeof(Packet));
    *pkt = (Packet){.len = 0, .data = NULL};
    return pkt;
}

void packet_free(Packet* pkt) {
    if (pkt) {
        if(pkt->data) {
            free(pkt->data);
        }
        free(pkt);
    }
}

enum SoF_SeekState {
    SEEKING, FOUND_MKR_0, FOUND_MKR_1
};

int seek_to_sof(FILE* stream) {
    char seek_state = SEEKING;

    // Scan through the stream until the two marker bytes are found consecutively.
    while (!feof(stream)) {
        int cur_byte = fgetc(stream);
        // printf("%X ", cur_byte);

        switch (seek_state) {
            case SEEKING:
                seek_state = (cur_byte == MKR_START_0)? FOUND_MKR_0 : SEEKING;
                break;

            case FOUND_MKR_0:
                // If we've found the first marker, and we're really at the
                // beginning of a packet, the next byte should be the second
                // marker. If it isn't then we're not looking at a real packet
                // and we're back to looking for the marker.
                seek_state = (cur_byte == MKR_START_1) ? FOUND_MKR_1 : SEEKING;
                break;
        }

        if (seek_state == FOUND_MKR_1)
            return 1;
    }

    return 0;
}


int read_len(FILE* stream, uint8_t* data_len) {
    int res = fgetc(stream);
    if (res != EOF) {
        *data_len = res;
    } else  {
        fprintf(stderr, "[ERR] Failed to read pkt len. Reached EOF\n");
    }
    return res != EOF;
}

int read_data(FILE* stream, Packet* pkt) {
    pkt->data = malloc(pkt->len * sizeof(uint8_t));
    int bytes_read = fread(pkt->data, 1, pkt->len, stream);
    if (bytes_read != pkt->len) {
        // If fread did not reaturn enough bytes, we should assume that the
        // stream has been interrupted.
        fprintf(stderr, "[ERR] fread expected %d bytes, received %d\n", pkt->len, bytes_read);
        return 0;
    }

    return 1;
}

int packet_read(FILE* stream, Packet* pkt) {
    assert(pkt && stream);

    if (! seek_to_sof(stream)) {
        pkt = NULL;
        return 0;
    }

    if (!read_len(stream, &pkt->len)) {
        pkt->len = 0;
        fprintf(stderr, "[ERR] Unable to read pkt length.\n");
        return 0;
    }

    // If pkt->len is set to 0 then calling read_data is a bit redundant but
    // it avoids special casing the logic.
    return read_data(stream, pkt);
}

void packet_print(Packet* pkt) {
    assert(pkt);

    printf("{%3d} ", pkt->len);
    for (int idx = 0; idx < pkt->len; ++idx) {
        printf("%X ", pkt->data[idx]);
    }
    printf("\n");
}
