#include <stdio.h>

#include "packet.h"


int main(int argc, char **argv) {
    if (argc > 1) {
        printf(
            "Read a binary stream from stdin and print valid packets to "
            "sdout.\n"
            "\n"
            "USAGE:\n"
            "  packet-processor < capture_file\n"
            "\n"
            "PACKET FORMAT:\n"
            "\n"
            "  +-------------+--------+---------+\n"
            "  |     SoF     |  LEN   |  DATA   |\n"
            "  +-------------+--------+---------+\n"
            "  | 0x21 | 0x22 | 1 BYTE | N BYTES |\n"
            "  +-------------+--------+---------+\n"
        );
        return 1;
    }

    // Re-open stdin as a binary file
    freopen(NULL, "rb", stdin);
    printf("Waiting for input ...\n");


    while (!feof(stdin)) {
        Packet *pkt = packet_new();
        if (packet_read(stdin, pkt)) {
            packet_print(pkt);
        } else if (!feof(stdin)) {
            fprintf(stderr, "Failed to read packet. \n");
        }
        packet_free(pkt);
    }

    return 0;
}
